import React, { Component } from 'react';
import { Container, Content, Header, Item, Input, Icon, List, ListItem, Text, Body } from 'native-base';

export default class Search extends Component {
    constructor() {
        super();

        this.timer = null;
        this.state = {
            listSize: 1,
        };
    }

    render() {
        const { listSize } = this.state;
        const { list, showUser, getListWithFilter } = this.props;

        const setCurrentReadOffset = (e) => {
            const offset = e.nativeEvent.contentOffset.y;
            const layoutHeight = e.nativeEvent.layoutMeasurement.height;
            const contentSize = e.nativeEvent.contentSize.height;

            if (offset + layoutHeight * 2 > contentSize)
                this.setState({ listSize: listSize + 1 });
        }

        return (
            <Container>
                <Header searchBar rounded>
                    <Item>
                        <Icon name="ios-search" />
                        <Input placeholder="Search" onChangeText={val => {
                            getListWithFilter(val);
                            this.setState({ listSize: 1 });
                        }} />
                        <Icon name="ios-people" />
                    </Item>
                </Header>
                <Content scrollEventThrottle={300} onScroll={setCurrentReadOffset}>
                    <List>
                        {list.slice(0, listSize * 20).map((d, k) => (
                            <ListItem
                                button key={`list-${k}`}
                                onPress={() => {
                                    showUser(d);
                                }}>
                                <Body>
                                    <Text style={d.checkedIn ? { color: 'green' } : {}}>{d.fullName}</Text>
                                    <Text style={d.checkedIn ? { color: 'green' } : {}} note numberOfLines={1}>{d.status}</Text>
                                </Body>
                            </ListItem>
                        ))}
                    </List>
                </Content>
            </Container>
        )
    }
}