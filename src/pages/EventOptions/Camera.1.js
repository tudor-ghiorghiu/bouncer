import React from 'react';
import { View, Keyboard, ToastAndroid } from 'react-native';
import { CameraKitCameraScreen, } from 'react-native-camera-kit';

export default ({ onBarcodeScan, notFound, person }) => {
    Keyboard.dismiss();

    return (
        <View>
            <CameraKitCameraScreen
                showFrame={true}
                scanBarcode={true}
                laserColor={'#1893d1'}
                frameColor={'#1893d1'}
                colorForScannerFrame={'#1893d1'}
                onReadCode={event =>
                    onBarcodeScan(event.nativeEvent.codeStringValue)
                }
            />
        </View>
    )
}