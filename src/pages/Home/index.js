import React, { Fragment } from 'react';

import { EventCard } from '../../organisms'
import events from './events.json';

import Checkin from '../EventOptions/Checkin';

export const Home = ({ navigation }) => (
    <Fragment>
        {
            events.map(e => <EventCard
                button
                key={e.id}
                data={e}
                pressEvent={() => navigation.navigate('EventOptions', e)}
            />)
        }
    </Fragment>
)

Home.navigationOptions = {
    title: 'EVENTS LIST',
    headerStyle: {
        backgroundColor: '#fefefe',
    },
    headerTintColor: '#000',
    headerTitleStyle: {
        fontWeight: 'bold',
    },
}