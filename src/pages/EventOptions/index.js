import React, { Component, useState } from 'react';
import { PermissionsAndroid, Platform, ToastAndroid, Vibration } from 'react-native';
import { Container, Tab, Tabs, TabHeading, Icon, Text } from 'native-base';

import Camera from './Camera';
import Checkin from './Checkin';
import Search from './Search';

import { saveCheckedIn, getCheckedIn } from '../../helpers/storage';
import allData from '../../data/accesa.json';

export class EventOptions extends Component {
  constructor() {
    super();

    this.state = {
      qrValue: null,
      person: null,
      list: allData,
      filter: '',
    }
  }

  _getListWithFilter(filter) {
    console.log(filter);
    return !filter ? allData :
      allData.filter(d => {
        return d.fullName.toLowerCase().includes(filter.toLowerCase()) ||
          d.status.toLowerCase().includes(filter.toLowerCase());
      });
  }

  _syncCheckedIn() {
    const { list, filter } = this.state;
    getCheckedIn().then((checkedInPpl) => {
      const data = list.map(d => {
        return { ...d, checkedIn: checkedInPpl.includes(`${d.id}`) };
      });
      this.setState({ list: data });
    });
  }

  componentDidMount() {
    this._syncCheckedIn();
    this.timer = setInterval(() => this._syncCheckedIn(), 5000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render() {
    const { qrValue, person, list, filter } = this.state;

    const displayList = !filter ?
      list :
      list.filter(d => {
        return d.fullName.toLowerCase().includes(filter.toLowerCase()) ||
          d.status.toLowerCase().includes(filter.toLowerCase());
      })

    if (qrValue && !person) {
      const personSearch = allData.filter(d => d.code === qrValue);
      Vibration.vibrate(100);

      if (!personSearch.length) ToastAndroid.showWithGravityAndOffset(
        'Invalid QR code or person not found',
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        25,
        50,
      );
      else this.setState({ person: personSearch[0] });
    }

    const onBarcodeScan = val => this.setState({ qrValue: val });

    const resetModal = () => {
      this.setState({ qrValue: null, person: null });
    }

    const checkin = async (id) => {
      await saveCheckedIn(id);
      await this._syncCheckedIn();
      resetModal();
    }

    // const onScannerOpened = async () => {
    //   if (Platform.OS === 'android') {
    //     try {
    //       await PermissionsAndroid.request(
    //         PermissionsAndroid.PERMISSIONS.CAMERA, {
    //           'title': 'The Bouncer Camera Permission',
    //           'message': 'The Bouncer needs access to your camera'
    //         }
    //       )
    //     } catch (err) {
    //       alert("Camera permission err", err);
    //       console.warn(err);
    //     }
    //   }
    // };
    // onScannerOpened();

    const tab1Heading = (
      <TabHeading>
        <Icon name="ios-search" />
        <Text>Search</Text>
      </TabHeading>
    )
    const tab2Heading = (
      <TabHeading>
        <Icon name="ios-camera" />
        <Text>Camera</Text>
      </TabHeading>
    );

    return (
      <Container>

        {(qrValue && person) && <Checkin person={person} resetModal={resetModal} checkin={checkin} />}

        <Tabs>
          <Tab heading={tab1Heading}>
            <Search
              showUser={(p) => this.setState({ qrValue: p.code })}
              list={displayList}
              getListWithFilter={filter => this.setState({ filter })} />
          </Tab>
          <Tab heading={tab2Heading}>
            <Camera onBarcodeScan={onBarcodeScan} />
          </Tab>
        </Tabs>
      </Container>
    )
  }
}

EventOptions.navigationOptions = {
  header: <></>
}