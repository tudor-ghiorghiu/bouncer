import React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { Content, Card, CardItem, Text, Button, Icon, Left, Body, Right, H1 } from 'native-base';

export const EventCard = ({ data, pressEvent }) => (
    <TouchableOpacity onPress={pressEvent}>
        <Card>
            <CardItem>
                <Left>
                    <Body>
                        <H1>{data.name}</H1>
                    </Body>
                </Left>
            </CardItem>
            <CardItem cardBody>
                <Image
                    source={{ uri: data.image }}
                    resizeMode="contain"
                    style={{ height: 200, backgroundColor: '#eee', width: null, flex: 1 }} />
            </CardItem>
            <CardItem>
                <Left>
                    <Button transparent>
                        <Icon active name="calendar" />
                        <Text>{data.period}</Text>
                    </Button>
                </Left>
                <Right>
                    <Button transparent>
                        <Icon active name="pin" />
                        <Text>{data.location}</Text>
                    </Button>
                </Right>
            </CardItem>
        </Card>
    </TouchableOpacity>
);