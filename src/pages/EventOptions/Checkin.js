import React from 'react';
import { Container, Content, Card, CardItem, Text, Body, Button, Left, Right } from 'native-base';
import { Modal } from 'react-native';

export default ({ person, resetModal, checkin }) => {
  return (
    <Modal
      animationType="slide"
      transparent={false}
      visible={true}
      onRequestClose={() => {
        console.log('requestClose')
      }}>
      <Container>
        <Content contentContainerStyle={{ justifyContent: 'center', flex: 1 }}>
          <Card style={{ alignItems: 'center' }}>
            <CardItem header>
              <Text style={{ fontSize: 25 }}> {person.fullName}</Text>
            </CardItem>

            <CardItem></CardItem>

            {person.status !== 'angajat' && <CardItem footer >
              <Text style={{ fontSize: 30, color: 'red' }}>Special GIFT!</Text>
            </CardItem>}

            <CardItem></CardItem>
            <CardItem>
              <Left>
                <Button success onPress={() => checkin(person.id)}>
                  <Text>Check in</Text>
                </Button>
              </Left>
              <Right>
                <Button danger onPress={() => resetModal()}>
                  <Text>Back</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>
        </Content>
      </Container>
    </Modal>
  )

  return (
    <Container>
      <Content>
        <Card>
          <CardItem header>
            <Text>NativeBase</Text>
          </CardItem>
          <CardItem>
            <Body>
              <Text>
              //Your text here
              </Text>
            </Body>
          </CardItem>
          <CardItem footer>
            <Text>GeekyAnts</Text>
          </CardItem>
        </Card>
      </Content>
    </Container>
  );
}