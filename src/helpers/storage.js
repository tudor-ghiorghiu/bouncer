import { AsyncStorage } from 'react-native';
import axios from 'axios';

export const getCheckedIn = async () => {
    const value = await AsyncStorage.getItem('checkedin');
    axios.get(`https://why-not.us/accesa.php`)
        .then(({ data }) => {
            AsyncStorage.setItem('checkedin', JSON.stringify(data));
        });
    return !!value ? JSON.parse(value) : [];
}

export const saveCheckedIn = async (id) => {
    const currentUsers = await getCheckedIn();

    if (currentUsers.includes(id)) return Promise.resolve();
    axios.get(`https://why-not.us/accesa.php?action=save&id=${id}`);

    const dataToSave = [...currentUsers, `${id}`];
    return AsyncStorage.setItem('checkedin', JSON.stringify(dataToSave));
}