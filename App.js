import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';

import { EventOptions, Home } from './src/pages';

const AppNavigator = createStackNavigator(
  {
    EventOptions,
    Home
  },
  {
    initialRouteName: 'Home'
  }
);
export default createAppContainer(AppNavigator);